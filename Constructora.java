public class Constructora {
    // ---------------------------------------------
    // Atributos
    // ---------------------------------------------
    public int tiempo;
    public double capital;
    public double interes;
    public double interesSimple;
    public double interesCompuesto;
    // ---------------------------------------------
    
    // ---------------------------------------------
    // Constructor
    // ---------------------------------------------

    public Constructora(int tiempo, double capital, double interes, double interesSimple, double interesCompuesto) {
        this.tiempo = tiempo;
        this.capital = capital;
        this.interes = interes;
        this.interesSimple = interesSimple;
        this.interesCompuesto = interesCompuesto;
    }

    // ---------------------------------------------
    // Métodos
    // ---------------------------------------------

    public double calcularInteresSimple(int tiempo, double capital, double interes){
        interesSimple = tiempo*capital*interes;
        return interesSimple;
    }
    public double calcularInteresCompuesto(int tiempo, double capital, double interes){
        interesCompuesto = capital*(Math.pow(1+interes,tiempo)-1); //Math es para llamar la libreria de las matematicas, con ella podemos traer operaciones compuestas, llamamos el pow, que su funcion es elevar la base a un exponente
        return interesCompuesto;
    }
    public String compararInversion(int tiempo, double capital, double interes){
        double respuesta = calcularInteresCompuesto(tiempo, capital, interes)- calcularInteresSimple(tiempo, capital, interes);
        if (respuesta >0){
            return "La diferencia en el total de intereses generados para el proyecto, si escogemos entre evaluarlo a una tasa de interés Compuesto y evaluarlo a una tasa de interés Simple, asciende a la cifra de: "+respuesta;
        }
        else{
            return "Faltan datos para calcular la diferencia en el total de intereses generados para el proyecto.";
        }
    }
}